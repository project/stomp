<?php

declare(strict_types=1);

namespace Drupal\stomp\Drush\Commands;

use Drupal\stomp\Consumer\ConsumerInterface;
use Drupal\stomp\Consumer\Options;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

/**
 * A drush commands file to provide STOMP queue commands.
 */
final class QueueCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\stomp\Consumer\ConsumerInterface $consumer
   *   The consumer service.
   */
  public function __construct(
    private readonly ConsumerInterface $consumer,
  ) {
    parent::__construct();
  }

  /**
   * Runs a specific queue by name.
   *
   * @param string $queue
   *   The name of the queue.
   * @param array{lease-time: int|string, items-limit: int|string} $options
   *   The options.
   *
   * @return int
   *   The exit code.
   */
  #[CLI\Command(name: 'stomp:worker')]
  #[CLI\Argument(name: 'queue', description: 'The name of the queue to run.')]
  #[CLI\Option(name: 'lease-time', description: 'The maximum number of seconds that an item remains claimed.')]
  #[CLI\Option(name: 'items-limit', description: 'The maximum number of items allowed to run the queue.')]
  public function worker(
    string $queue,
    array $options = [
      'lease-time' => 3600,
      'items-limit' => 0,
    ],
  ) : int {

    try {
      $this->consumer
        ->process($queue,
          new Options(
            lease: (int) $options['lease-time'],
            itemLimit: (int) $options['items-limit']
          )
        );
    }
    catch (\Exception $e) {
      $this->io()->error($e->getMessage());

      return DrushCommands::EXIT_FAILURE;
    }
    return DrushCommands::EXIT_SUCCESS;
  }

}
