<?php

declare(strict_types=1);

namespace Drupal\Tests\stomp\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\stomp\Event\MessageEvent;
use Stomp\Transport\Bytes;
use Stomp\Transport\Map;
use Stomp\Transport\Message;

/**
 * Tests message event.
 */
class MessageEventTest extends UnitTestCase {

  /**
   * Tests ::create().
   *
   * @param mixed $body
   *   The body.
   * @param class-string<object> $expectedClass
   *   The expected class.
   *
   * @dataProvider createData
   */
  public function testCreateMethod(mixed $body, string $expectedClass) : void {
    $sut = MessageEvent::create($body);
    $this->assertInstanceOf($expectedClass, $sut->message);
  }

  /**
   * Tests an invalid message type.
   */
  public function testException() : void {
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Invalid message type.');
    MessageEvent::create(new \stdClass());
  }

  /**
   * Test data for message event.
   *
   * @return array<mixed>
   *   The data.
   */
  public static function createData() : array {
    return [
      ['body', Message::class],
      [['body'], Map::class],
      [new Message('123'), Message::class],
      [new Bytes('123'), Bytes::class],
    ];
  }

}
